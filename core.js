/**
 * $byview 用于cms的一个基于vue的前端视图微框架
 * 采用vue取缔cms系统页面中原来的前后端分离模式, 原cms的前后端分离模式
 * 缺少组件功能, 且大部分操作需要依赖jQuery或者dom操作, 效率远不如vue
 * 且容易出错, 开发和调试难度大
 *
 * $byview 的主要功能为:
 *
 *  - 集成 ant design vue 前端视图框架
 *  - loadComponent() 异步加载,同步返回vue组件
 *  - loadScript() 异步加载,同步或者异步执行外部脚本
 *  - run() 启动应用
 *  - loadCss() 加载css文件
 *
 *  所有的vue组件以 .vue 结尾, 所有异步加载资源的方法均支持自动追加cms的网站路径
 *  原cms功能, 在vue中依旧可以使用, 例如 Msg.doing, 但是不推荐使用
 *
 *  $byview 在使用的执行顺序为:
 *      1. 加载 $byview/core.js, 这一步需要使用 $.getScript
 *      2. 在原cms应用入口文件中执行, $byview.run 方法指定当前应用的vue基础组件(可以理解为layout)
 *      4. 在vue基础组件中定义template(模板), script(执行脚本), style(样式).
 *      5. 在vue基础组件script中(该基础组件可以不需要name属性), 调用 $byview.loadComponent 或者
 *         $byview.loadComponentGroupByName 加载子组件, 可以通过await来同步获取组件.
 *      6. 在vue基础组件script中通过export default语句导出对象, 与vue-cli中开发vue应用类似

 * @author JinkoWu
 * @createdAt 2021-01-29
 */
(window => {
    let webRootUrl = (window.WEBROOT_URL || '/').replace(/\/+$/, '') + '/';
    let byviewUrl = (window.BYVIEW_URL || webRootUrl + 'byview').replace(/\/+$/, '') + '/';
    
    let _config = {
        baseUrl: webRootUrl,
        resourceVersion: window.BYVIEW_RES_VERSION || '',
        enabledScriptSuperCache: false,
        loadScripts: [
            {url: byviewUrl + 'vendor/dayjs/1.4.1/dayjs.min.js',once: true, randkey:true},
            {url: byviewUrl + 'vendor/vue/v3/3.2.6/index.js', once: true, randkey:true},
            {url: byviewUrl + 'vendor/element-plus/v1.1.0-beta7/index.full.js', once: true, randkey:true},
            {url: byviewUrl + 'vendor/JKHTMLParser.js', once: true, randkey:true},
            {url: byviewUrl + 'core/request.js', once: true, randkey:true},
        ],
        
        loadCss: [
            byviewUrl + 'vendor/element-plus/v1.1.0-beta7/index.css'
        ],
    
        //不增加scopedId属性的tag
        scopedIdIgnoreTags: ['br'],
        topframe: true
    };
    
    window.$byview = window.$byview ? window.$byview : {};
    $byview.$var = {};
    
    let util = (_ => {
        $byview.util = $byview.util ? $byview.util : {};
        let _type = v => Object.prototype.toString.call(v);
        let $this = null;
        let unid = (new Date).getTime();
    
        class ByviewUtil  {
            constructor() {
                this._data = {
                    VALUE_RAW_TYPE: {
                        OBJECT: _type({}),
                            ARRAY: _type([]),
                            REGEXP: _type(/1/),
                            PROMISE: _type(Promise.resolve()),
                    },
        
                    _uniqidIncrements: {}
                };
    
                $this = this;
            }
            
            getValueRawType(v) {return _type(v)}
            isArray(v)      {return _type(v) === $this._data.VALUE_RAW_TYPE.ARRAY}
            isObject(v)      {return _type(v) === $this._data.VALUE_RAW_TYPE.OBJECT}
            isRegExp(v)     {return _type(v) === $this._data.VALUE_RAW_TYPE.REGEXP}
            isPromise(v)    {return _type(v) === $this._data.VALUE_RAW_TYPE.PROMISE}
            isFunction(v)   {return typeof v === 'function'}
            isBoolean(v)    {return typeof v === 'boolean'}
            isNaN(v)        {return isNaN(v)}
            isNumber(v)     {return typeof v === 'number'}
            isString(v)     {return typeof v === 'string'}
            isNull(v)       {return v===null}
            isUndefined(v)   {return typeof v === 'undefined'}
            
            isEmpty(v) {
                return (
                    v===false || $this.isUndefined(v) || $this.isNull(v) ||
                    v === '' || ($this.isArray(v) && v.length==0)
                );
            }
            
            isEmptyMixed(v) {
                if($this.isArray(v)) {
                    return v.length == 0;
                } else if($this.isObject(v)) {
                    return Object.keys(v).length == 0;
                }
                
                return false;
            }
            
            isFalseValue(v) {
                return $this.isEmpty(v) || v === 0;
            }
            
            isTrueValue(v) {
                return !$this.isFalseValue(v);
            }
            
            getDefaultWhenEmpty(v, defaultValue) {
                return $this.isEmpty(v) ? defaultValue : v;
            }
            
            convertToArray(value) {
                return Array.prototype.slice.call(value);
            }
    
            /**
             * @param obj
             */
            clone(obj) {
                return JSON.parse(JSON.stringify(obj));
            }
            
            /**
             * 生成唯一id
             * @param unidKey
             * @param incrementMinLength
             * @returns {string}
             */
            createUnqid(unidKey, incrementMinLength) {
                unidKey = $this.getDefaultWhenEmpty(unidKey, '');
                incrementMinLength = $this.getDefaultWhenEmpty(incrementMinLength, 3)
                
                $this._data._uniqidIncrements[unidKey] = $this._data._uniqidIncrements[unidKey] || 1;
                let value = $this._data._uniqidIncrements[unidKey]++;
                
                if(incrementMinLength && String(value).length < incrementMinLength) {
                    value = '0'.repeat(incrementMinLength - String(value).length) + value;
                }
                
                return String(parseInt(new Date().getTime()/1000)) + value;
            }
            
            /**
             * 执行js代码
             * @param scriptContent
             * @param sourcePath
             */
            eval(scriptContent, sourcePath) {
                if(sourcePath) {
                    scriptContent = `${scriptContent}\n//# sourceURL=byview://${sourcePath}`;
                }
                
                return window.eval.call(window, scriptContent);
            }
            
            urlPath() {
                let args = $this.convertToArray(arguments);
                let last = args[args.length-1];
                let prefix = (args[0][0] == '/' || args[0][0] == '\\') ? '/' : '';
                return prefix + args.map(r=>String(r).trim().replace(/^\/|\/$/g, '')).filter(a=>a||a===0).join('/') + (/\/$/.test(last)?'/':'');
            }
            
            /**
             * 格式化url
             * @param url
             * @returns {string}
             */
            formatUrl(url) {
                url = url.replace(/^\s+|\s$/g, '');
                let queryPos = url.indexOf('?');
                let hashPos = url.indexOf('#');
                let targetPos = queryPos == -1 ? hashPos : (hashPos == -1 ? queryPos : Math.min(queryPos, hashPos));
                let remain = '';
                
                // #或?后面的不处理
                if(targetPos != -1) {
                    remain = url.substr(targetPos);
                    url = url.substr(0, targetPos);
                }
                
                url = url.replace(/\\+/, '/');
                
                if(/^[a-z0-9]+:\//i.test(url)) {
                    //带协议
                    url = url.replace(/^([a-z0-9]+:)\//i, '$1#');
                } else if(/^\/\//.test(url)) {
                    //使用当前协议
                    url = '#' + url.substr(2);
                }
                
                // 合并多个/
                url = url.replace(/\/{2,}/g, '/');
                
                // 还原协议中的/
                return url.replace('#', '//').replace('///', '//') + remain;
            }
            
            /**
             * 补全url, 若url不是一个完整的url
             * 则自动补全base
             * @param url
             * @param base
             * @returns {*}
             */
            completeUrl(url, base) {
                url = $this.formatUrl(url);
                
                if(!/^(([a-z0-9]+:\/)|\/)/i.test(url)) {
                    return $this.urlPath(base, url);
                }
                
                return url;
            }
            
            /**
             * 获取url中的查询参数
             * @param url
             * @returns {string}
             */
            getUrlQuery(url) {
                let qpos = url.indexOf('?');
                let hpos = url.indexOf('#');
                
                if(qpos == -1 || (hpos != -1 && qpos > hpos)) {
                    return "";
                } else {
                    if(hpos == -1) {
                        return url.substr(qpos+1)
                    } else {
                        return url.substr(qpos+1, hpos-qpos-1);
                    }
                }
            }
            
            /**
             * 解析url的查询参数
             * @param query
             * @returns {*}
             */
            parseUrlQuery(query) {
                if(query.indexOf('?') != -1) {
                    query = $this.getUrlQuery(query);
                }
                
                let paramGroups = query.split('&');
                let params = {};
                let counters = {};
                let objType = Object.prototype.toString.call({});
                
                for(let paramstr of paramGroups) {
                    let eqpos = paramstr.indexOf('=');
                    let paramName, paramVal=null;
                    
                    if(eqpos != -1) {
                        paramName = decodeURIComponent(paramstr.substr(0, eqpos));
                        paramVal = decodeURIComponent(paramstr.substr(eqpos+1));
                    } else {
                        paramName = decodeURIComponent(paramstr);
                    }
                    
                    paramName = paramName.trim();
                    
                    if(paramName) {
                        if(/^.*?(\[.*?\])+$/.test(paramName)) {
                            let keys = paramName.split('[').map(r=>r.replace(/^\[+|\]+$/g, ''));
                            let pointer = params, counterName = '';
                            
                            for(let i in keys) {
                                let key = keys[i];
                                counterName += `[${key}]`;
                                
                                //最后一个key, 需要写入数据
                                if(i == keys.length-1) {
                                    if(key === '') {
                                        key = (counters[counterName]||0===counters[counterName]) ? ++counters[counterName] : counters[counterName]=0;
                                        pointer.__isArray = true;
                                        pointer[key] = paramVal;
                                        pointer.length = pointer.length?pointer.length+1:1;
                                    } else {
                                        pointer[key] = paramVal;
                                    }
                                    
                                    break;
                                }
                                
                                if(key === '') {
                                    pointer.__isArray = true;
                                    key = (counters[counterName]||0===counters[counterName]) ? ++counters[counterName] : counters[counterName]=0;
                                    pointer[key] = {};
                                    pointer.length = pointer.length ? pointer.length + 1: 1;
                                } else  if(Object.prototype.toString.call(pointer[key]) != objType) {
                                    pointer[key] = {};
                                }
                                
                                pointer = pointer[key];
                            }
                        } else {
                            params[paramName] = paramVal;
                        }
                    }
                }
                
                let _convert = items => {
                    let isary = items && items.__isArray;
                    
                    if(isary) {
                        items = Array.prototype.slice.call(items);
                    }
                    
                    if(isary || Object.prototype.toString.call(items) == objType) {
                        for(let k in items) {
                            items[k] = _convert(items[k]);
                        }
                    }
                    
                    return items;
                }
                
                return _convert(params);
            }
            
            /**
             * 组装url的查询参数
             * @param params
             * @param raw
             * @returns {string|[]}
             */
            unparseUrlQuery(params, raw) {
                let
                    aryType = Object.prototype.toString.call([]),
                    objType = Object.prototype.toString.call({}),
                    expressions = {}
                ;
                
                params = params || {};
                
                let simpledecode = str => str.replace('&', '%26');
                
                /**
                 * 遍历
                 * @param prefix 前缀
                 * @param items 子元素
                 * @private
                 */
                let _map = (prefix,items) => {
                    let type = Object.prototype.toString.call(items);
                    prefix = prefix || '';
                    
                    // if(type == objType || type == aryType) {
                    if(type == objType) {
                        for(let key in items) {
                            if(prefix) {
                                _map(prefix + (type==aryType ? '[]' : `[${key}]`), items[key]);
                            } else {
                                _map(key, items[key]);
                            }
                        }
                    } else {
                        //叶子节点
                        let val;
                        
                        if(type === aryType) {
                            prefix += '[]';
                            val = items;
                        } else if(typeof items == 'boolean') {
                            val = items ? '1' : '0';
                        } else if(typeof items === 'undefined') {
                            val = items;
                        } else if(items === null) {
                            val = null;
                        } else if(items instanceof File) {
                            val = items;
                        } else {
                            val = String(items);
                        }
                        
                        expressions[simpledecode(prefix)] = val;
                    }
                }
                
                _map("", params);
                
                let ary = [];
                let rawAry = [];
                
                for(let key in expressions) {
                    //undefined 为删除参数
                    if(typeof expressions[key] === 'undefined') {
                        continue;
                    }
                    
                    //数组
                    if(Object.prototype.toString.call(expressions[key]) === aryType) {
                        for(let aryValue of expressions[key]) {
                            if(typeof aryValue === 'undefined') {
                                continue;
                            }
                            
                            ary.push(key+'='+encodeURIComponent(String(aryValue)));
                            rawAry.push({key, value:String(aryValue)});
                        }
                        
                        continue;
                    }
                    
                    ary.push(key + '' + (expressions[key] === null ? "" : "="+encodeURIComponent(expressions[key])));
                    rawAry.push({key, value:expressions[key]})
                }
                
                if(raw) {
                    return rawAry;
                }
                
                return ary.join('&');
            }
            
            /**
             * 获取url hash里的查询参数
             * @param url
             * @returns {string}
             */
            getUrlHashQuery(url) {
                let hpos = url.indexOf('#');
                
                if(hpos == -1) {
                    return "";
                }
                
                let qpos = url.indexOf('?', hpos+1);
                
                if(qpos == -1) {
                    return "";
                } else {
                    return url.substr(qpos+1)
                }
            }
            
            /**
             * 解析url hash
             * 分为 路径path, appSign, 查询参数params
             * @param url
             * @returns {{path: string, sign: string, params: {}}}
             */
            parseUrlHash(url) {
                let hpos = url.indexOf('#');
                
                if(hpos == -1) {
                    return {sign:'', path:'', params:{}};
                }
                
                let hash = url.substr(hpos+1);
                let qpos = hash.indexOf('?');
                let hashQueryStr = '';
                
                if(qpos != -1) {
                    hashQueryStr = hash.substr(qpos+1);
                    hash = hash.substr(0, qpos);
                }
                
                let cpos = hash.indexOf(':');
                let sign, hashPath;
                
                if(cpos != -1) {
                    sign = hash.substr(0, cpos);
                    hashPath = hash.substr(cpos+1);
                } else {
                    sign = hash;
                    hashPath = '';
                }
                
                let hashQueryParams = {};
                
                if(hashQueryStr) {
                    hashQueryParams = $this.parseUrlQuery(hashQueryStr);
                }
                
                return {
                    sign,
                    path:hashPath,
                    params: hashQueryParams
                }
            }
            
            /**
             * 组装url hash
             * @param hashInfo
             * @returns {string}
             */
            unparseUrlHash(hashInfo) {
                let {sign, path, params} = hashInfo;
                sign = sign || '';
                path = path || '';
                params = params || {};
                
                let hash = sign + (path ? ':' + path: '');
                let query = $this.unparseUrlQuery(params||{});
                
                if(query) {
                    hash += '?' + query;
                }
                
                return hash;
            }
    
            updateUrlQuery(url, queryParams, noMerge) {
                url = url || "";
                let pos = url.indexOf('?');
                let hpos = url.indexOf('#');
                queryParams = queryParams || {};
        
                if(hpos !== -1 && pos > hpos) {
                    pos = -1;
                }
        
                let _combine = (prefix,str) => str ? prefix+str:str;
        
                if(!noMerge) {
                    let params = $this.parseUrlQuery($this.getUrlQuery(url));
                    queryParams = Object.assign(params, queryParams);
                }
        
                if(pos == -1) {
                    if(hpos !== -1) {
                        return url.substr(0, hpos) + _combine('?', $this.unparseUrlQuery(queryParams)) + url.substr(hpos);
                    } else {
                        return url + _combine('?', $this.unparseUrlQuery(queryParams));
                    }
                } else if(hpos !== -1) {
                        return url.substr(0, pos) + _combine('?', $this.unparseUrlQuery(queryParams)) + url.substr(hpos);
                } else {
                    return url.substr(0, pos) + _combine('?', $this.unparseUrlQuery(queryParams));
                }
            }
            
            /**
             * 更新url hash 中的sign
             * @param url
             * @param sign
             * @returns {string}
             */
            updateUrlHashSign(url, sign) {
                let hpos = url.indexOf('#');
                
                if(hpos == -1) {
                    return url +'#'+ $this.unparseUrlHash({sign});
                }
                
                let hashInfo = $this.parseUrlHash(url);
                hashInfo.sign = sign;
                return url.substr(0, hpos) + '#' + $this.unparseUrlHash(hashInfo);
            }
            
            /**
             * 更新url hash 中的path
             * @param url
             * @param path
             * @returns {string}
             */
            updateUrlHashPath(url, path) {
                let hpos = url.indexOf('#');
                
                if(hpos == -1) {
                    return url +'#'+ $this.unparseUrlHash({path});
                }
                
                let hashInfo = $this.parseUrlHash(url);
                hashInfo.path = path;
                return url.substr(0, hpos) + '#' + $this.unparseUrlHash(hashInfo);
            }
            
            /**
             * 更新url hash 中的params
             * @param url
             * @param params
             * @param noMerge
             * @returns {string}
             */
            updateUrlHashQuery(url, params, noMerge) {
                params = params || {};
                let hpos = url.indexOf('#');
                
                if(hpos == -1) {
                    return url +'#'+ $this.unparseUrlHash({params});
                }
                
                let hashInfo = $this.parseUrlHash(url);
                
                if(noMerge) {
                    hashInfo.params = params;
                } else {
                    hashInfo.params = Object.assign(hashInfo.params, params);
                }
                
                return url.substr(0, hpos) + '#' + $this.unparseUrlHash(hashInfo);
            }
            
            /**
             * 随机字符串
             * @returns {string}
             */
            randomStr() {
                return String(Math.random()).substr(2);
            }
            
            /**
             * 获取浏览器信息
             * @returns {{versionSegments: *, type: *, version: *}|boolean}
             */
            getBrowserInfo() {
                let userAgent = navigator.userAgent;
                let getRegExp = type => {
                    return new RegExp(` ${type}/([0-9]+(?:.[0-9]+)*?) `, 'i');
                };
                
                let genRet = (type, version) => {
                    version = version ? version : "0";
                    
                    return {
                        type,
                        version,
                        versionSegments: version.split('.').map(v => parseInt(v))
                    }
                };
                
                let find = type => {
                    if(getRegExp(type).test(userAgent)) {
                        return genRet(type, userAgent.match(getRegExp(type))[1])
                    }
                    
                    return false;
                };
                
                let defaultResult = genRet('unknown', '0');
                
                return find('chrome')
                    || find('firefox')
                    || find('edg')
                    || find('safari')
                    || defaultResult
                    ;
            }
    
            /**
             * 防抖
             * @param callback
             * @param option
             * @returns {Function}
             * fn.raw 为原始函数
             */
            antiShake(callback, option) {
                option = option || {};
                let delay = option.delay || 50;
                
                return (function() {
                    let timer = null;
                    
                    let fn = function() {
                        clearTimeout(timer);
                        timer = setTimeout(callback, delay);
                    };
                    
                    fn.raw = callback;
                    
                    return fn;
                })();
            }
            
            /**
             * 格式化前端数据(根据结构字段, 数值类型等约定)
             *
             * 若format的格式如果是空对象 {} 则会忽略removeKey
             *  若非空对象, 例如 {test:1} 则指定removeKey为true的情况下, 该对象只能有test一个key
             *  其他key会被移除
             * 若format中的数组指定了成员, 例如 [1,2,2] 则在removekey 下, 数组长度固定
             * 若数组第一个成员为函数, 则认为该函数的返回值为数组对象成员格式化参数, 例如 [_=> ({test:1})]
             *  表示数组中的所有成员都是 {test:1} 格式
             *
             * 若格式中的对应有 '__' 成员且是函数的话, 那么该对象会以该函数返回值作为所有成员的格式(用于针对,动态键值的对象)
             * 示例: format= {
             *              __: _=>({score:1, test:''})
             *              }
             * formatData(format, {a:1}) => {a:{score:1, test:''}}
             *
             * format里所有值的数据为默认数据, 当格式不满足时会取默认数据
             *   Number和String类型数据不会严格判断类型, 会直接转换类型, 包括null, true, false
             *
             * @callback IByviewHelper_formatData
             * @param format
             * @param data
             * @param removeKey 强制按照format格式里的key, 移除不在format里存在的key
             * @returns {[]|*[]|*}
             */
            formatData(format, data, removeKey) {
                let getVarType = v => Object.prototype.toString.call(v);
                let tary = getVarType([]);
                let tobj = getVarType({});
                let tnum = getVarType(0);
                let tstr = getVarType('');
                let tnull = getVarType(null);
                let tbool = getVarType(false);
                let tfun = getVarType(_=>_);
        
                let isSameVarType = (v1, v2) => {
                    return getVarType(v1) === getVarType(v2);
                }
        
                //复杂数据 数组,对象 等
                let isComplexData  = v => getVarType(v) === tobj || getVarType(v) === tary ;
        
                //基础类型数据格式化
                let formatBaseTypeValue = (f, d) => {
                    if(!isSameVarType(f, d)) {
                        let v;
                        switch(getVarType(f)) {
                            case tnum:
                                if([tstr, tnull, tbool].includes(getVarType(d))) {
                                    v = Number(d);
                                } else {
                                    v = f;
                                }
                        
                                d = isNaN(v) ? f : v;
                                break;
                    
                            case tstr:
                                if([tnum, tnull, tbool].includes(getVarType(d))) {
                                    v = Number(d);
                                } else {
                                    v = f;
                                }
                        
                                d = String(v);
                                break;
                        }
                    }
            
                    return d;
                };
        
                if(!isComplexData(format)) {
                    return formatBaseTypeValue(format, data);
                }
        
                let transformFormatToData = f => {
                    if(isComplexData(f)) {
                        let newFormat = getVarType(f) == tary ? [] : {};
                
                        if(getVarType(f) == tary && getVarType(f[0]) == tfun) {
                            return [];
                        }
                
                        if(getVarType(f) == tobj && getVarType(f.__) == tfun) {
                            return {};
                        }
                
                        for(let i in f) {
                            newFormat[i] = transformFormatToData(f[i]);
                        }
                
                        return newFormat;
                    } else {
                        return f;
                    }
                };
        
                if(!isSameVarType(format, data)) {
                    if(getVarType(format) == tary && getVarType(format[0]) == tfun) {
                        return [];
                    }
            
                    return transformFormatToData(format);
                }
        
                //根数组
                if(
                    (getVarType(format) == tary && getVarType(format[0]) == tfun)
                    || (getVarType(format) == tobj && getVarType(format.__) == tfun)
                ) {
                    format = getVarType(format) == tobj ? format.__() : format[0]();
            
                    for(let i in data) {
                        data[i] = this.formatData(format, data[i], removeKey);
                    }
            
                    return data;
                }
        
                for(let i in format) {
                    if(!isComplexData(format[i])) {
                        //数据类型不一致
                        if(typeof data[i] === 'undefined' || isComplexData(data[i])) {
                            data[i] = transformFormatToData(format[i]);
                        } else {
                            data[i] = formatBaseTypeValue(format[i], data[i]);
                        }
                
                        continue;
                    }
            
                    if(!isSameVarType(format[i], data[i])) {
                        data[i] = transformFormatToData(format[i]);
                    } else if(getVarType(format[i]) === tobj) {
                        data[i] = this.formatData(format[i], data[i], removeKey);
                    } else if(getVarType(format[i]) === tary) {
                        if(getVarType(format[i][0]) === tfun) {
                            let childType = format[i][0]();
                            for(let j in data[i]) {
                                data[i][j] = this.formatData(childType, data[i][j], removeKey);
                            }
                        } else {
                            for(let j in data[i]) {
                                data[i][j] = this.formatData(format[i][j], data[i][j], removeKey);
                            }
                        }
                    }
                }
        
                if(removeKey) {
                    if(getVarType(data) === tobj) {
                        if(Object.keys(format).length > 0) {
                            for(let i in data) {
                                if(typeof format[i] === 'undefined') {
                                    delete data[i];
                                }
                            }
                        }
                    } else if(getVarType(data) === tary) {
                        let newData = [];
                
                        for(let i in data) {
                            if(typeof format[i] === 'undefined') {
                                continue;
                            }
                    
                            newData.push(data[i]);
                        }
                
                        data = newData;
                    }
                }
        
                return data;
            }
    
            /**
             * 返回上级目录
             * @param path
             * @returns {string}
             */
            dirname(path) {
                if(!path) {
                    return '';
                }
    
                path = path.replace(/\\/g, '/').replace(/\/+$/, '');
    
                if(path == '') {
                    return '/';
                }
                
                let pos = path.lastIndexOf('/');
                
                if(pos == -1) {
                    return '.';
                }
                
                let dir = path.substr(0, pos);
                dir = dir=='' ? '/' : dir;
                return dir;
            }
    
            /**
             * 首字母大写
             * @param str
             * @returns {*}
             */
            ucfirst(str) {
                return $this._lucfirst(str, true);
            }
            
            /**
             * 首字母小写
             * @param str
             * @returns {*}
             */
            lcfirst(str) {
                return $this._lucfirst(str, false);
            }
    
            _lucfirst(str, upper) {
                if(!str) {
                    return '';
                }
    
                let first = str.substr(0, 1);
                return (upper ? first.toUpperCase() : first.toLowerCase()) + str.substr(1);
            }
    
            /**
             * 获取路径中的的最后一节
             * @param path
             * @returns {string|*}
             */
            basename(path) {
                if(!path) {
                    return '';
                }
    
                path = path.replace(/\\/g, '/').replace(/\/+$/, '');
    
                if(path == '') {
                    return '';
                }
    
                let pos = path.lastIndexOf('/');
    
                if(pos == -1) {
                    return path;
                }
                
                return path.substr(pos+1);
            }
    
            /**
             * 获取文件名称, 不包含后缀
             * @param path
             * @returns {string|*}
             */
            filename(path) {
                path = $this.basename(path);
                let dot = path.lastIndexOf('.');
                
                if(dot == -1) {
                    return path;
                }
                
                return path.substr(0, dot);
            }
    
            /**
             * 去掉路径的后缀
             * @param path
             * @returns {string}
             */
            pathWithoutSubfix(path) {
                return [$this.dirname(path), $this.filename(path)].filter(_=>_).join('/');
            }
    
            /**
             * 复制文本
             * @param text
             */
            copyText(text) {
                let textInputId = "copyTextarea_" + unid;
                let elm = document.getElementById(textInputId);
                
                if(!elm) {
                    elm = document.createElement('textarea');
                    elm.style = "width:1px;height:1px;overflow:hidden;opacity:0.01;position:fixed;right:-10px;bottom:-10px;";
                    document.body.appendChild(elm);
                }
                
                elm.value = text;
                elm.select();
                let result = document.execCommand("copy");
                elm.blur();
                elm.value = '';
                return result;
            }
    
            filesub(path, includeDot) {
                let file = $this.basename(String(path));
                let pos = file.lastIndexOf('.');
        
                if(pos == -1) {
                    return '';
                }
        
                return file.substr(includeDot ? pos : pos + 1);
            }
    
            /**
             * 计算2个数组的交集
             * @param aryLeft
             * @param aryRight
             * @returns {*}
             */
            getIntersection(aryLeft, aryRight) {
                return aryLeft.filter(function(v){ return aryRight.indexOf(v) > -1 })
            }
    
            hasIntersection(aryLeft, aryRight) {
                return aryLeft.filter(function(v){ return aryRight.indexOf(v) > -1 }).length > 0;
            }
    
            filterObjectValues(obj, filterMethod, deep) {
                obj = obj || {};
                filterMethod = filterMethod || ((item, key) => !$this.isFalseValue(item));
        
                if($this.isArray(obj)) {
                    return obj.map(item => $this.filterObjectValues(item, filterMethod, deep));
                }
        
                let keys = Object.keys(obj);
        
                for(let key of keys) {
                    let item = obj[key];
            
                    if(deep && ($this.isObject(item) || $this.isArray(item))) {
                        obj[key] = $this.filterObjectValues(item, filterMethod, deep);
                    } else if(!filterMethod(item)) {
                        delete obj[key]
                    }
                }
        
                return obj;
            }
        };
        
        return $byview.util = new ByviewUtil;
    })();
    
    let superCache = (_ => {
        $byview.superCache = $byview.superCache ? $byview.superCache : {};
        return Object.assign($byview.superCache, {
            VERSION: 2,
            COMM_CACHE_STORE_NAME: 'comm_cache',
            RESOURCES_CACHE_STORE_NAME: 'resources_cache',
        
            db: null,
            ready: false,
        
            /**
             * 是否支持indexedDB
             */
            available() {
                return !!this.indexedDB();
            },
        
            indexedDB() {
                return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB;
            },
        
            async _transactionStore(storeName) {
                if(!await this.waitingForReady()) {
                    return false;
                }
            
                return this.db.transaction(storeName, 'readwrite').objectStore(storeName);
            },
        
            /**
             * 设置值
             * @param storeName
             * @param key
             * @param value
             * @param duration 有效生命时长, -1为不限制, 单位毫秒
             * @param falseWhenNoExist
             */
            async _set(storeName, key, value, duration, falseWhenNoExist) {
                let store = await this._transactionStore(storeName);
            
                if(!store) {
                    return false;
                }
            
                if(typeof duration == 'undefined' || duration === null || duration < 0) {
                    duration = -1;
                } else {
                    duration = parseInt(duration);
                }
            
                if(duration === 0) {
                    return false;
                }
            
                let invalidAt = duration == -1 ? false : new Date().getTime() + duration;
                let updateValue = {id: key, value, invalidAt: invalidAt};
            
                return await new Promise((ok) => {
                    let request;
                
                    if(falseWhenNoExist) {
                        request = store.add(updateValue);
                    } else {
                        request = store.put(updateValue);
                    }
                
                    request.onerror = function(e) {
                        console.warn('[SuperCache] set "' + key + '" fail', e.target.error, e);
                        ok(false);
                    };
                
                    request.onsuccess = function(e) {
                        ok(true);
                    };
                });
            },
        
            /**
             * 获取值
             * @param storeName
             * @param key
             * @param defaultValue
             */
            async _get(storeName, key, defaultValue) {
                let store = await this._transactionStore(storeName);
            
                if(!store) {
                    return defaultValue;
                }
            
                return await new Promise((ok) => {
                    let request = store.get(key);
                    request.onerror = function(e) {
                        console.warn('[SuperCache] get "' + key + '" fail', e.target.error, e);
                        ok(defaultValue);
                    };
                
                    request.onsuccess = function(e) {
                        let data = e.target.result ? e.target.result : null;
                        let value = defaultValue;
                    
                        if(!data) {
                            ok(defaultValue);
                            return;
                        }
                    
                        if(!data.invalidAt || new Date().getTime() <= data.invalidAt) {
                            value = data.value;
                        }
                    
                        value = !value&&value !== 0 ? defaultValue : value
                        ok(value);
                    };
                });
            },
        
        
        
            /**
             * 删除
             * @param storeName
             * @param key
             * @returns {Promise<unknown>}
             * @private
             */
            async _del(storeName, key) {
                let store = await this._transactionStore(storeName);
            
                if(!store) {
                    return false;
                }
            
                return await new Promise((ok) => {
                    let request = store.delete(key);
                
                    request.onerror = function(e) {
                        console.warn('[SuperCache] set "' + key + '" fail', e.target.error, e);
                        ok(false);
                    };
                
                    request.onsuccess = function(e) {
                        ok(true);
                    };
                });
            },
        
            /**
             * 获取通用缓存
             * @param key
             * @param defaultValue
             * @returns {Promise<unknown>}
             */
            async get(key, defaultValue) {
                return await this._get(this.COMM_CACHE_STORE_NAME, key, defaultValue);
            },
        
            async set(key, value, duration, falseWhenNoExist) {
                return await this._set(this.COMM_CACHE_STORE_NAME, key, value, duration, falseWhenNoExist);
            },
        
            /**
             * 删除
             * @param key
             * @returns {Promise<boolean|*>}
             */
            async del(key) {
                return await this._del(this.COMM_CACHE_STORE_NAME, key);
            },
        
            /**
             * 清空
             * @returns {Promise<unknown>}
             */
            async clearAll() {
                let store = await this._transactionStore(this.COMM_CACHE_STORE_NAME);
                let request = store.clear();
            
                return await new Promise((ok) => {
                    request.onerror = function (e) {
                        console.warn('[SuperCache] clear comm fail', e.target.error, e);
                        ok(false);
                    };
                
                    request.onsuccess = function (e) {
                        ok(true);
                    };
                });
            },
        
            /**
             * 获取资源缓存
             * @param prefix
             * @param path
             * @returns {Promise<unknown>}
             */
            async getResourceCache(prefix, path) {
                prefix = prefix ? prefix+'$/' : '';
                path = prefix + path;
                path = util.formatUrl(path);
                let result = await this._get(this.RESOURCES_CACHE_STORE_NAME, path);
                
                if(result) {
                    $byview.groupLog("%c[SuperCache] 缓存命中 " + path, "color:gray");
                }
                
                return result;
            },
        
            /**
             * 设置资源缓存
             * @param prefix
             * @param path
             * @param value
             * @param duration 有效生命时长, -1为不限制, 单位毫秒
             * @param falseWhenNoExist
             * @returns {Promise<unknown>}
             */
            async setResourceCache(prefix, path, value, duration, falseWhenNoExist) {
                prefix = prefix ? prefix+'$/' : '';
                path = prefix + path;
                path = util.formatUrl(path);
                console.log('%c[SuperCache] 缓存更新 ',  "color:green;", path);
                return await this._set(this.RESOURCES_CACHE_STORE_NAME, path, value, duration, falseWhenNoExist);
            },
        
            /**
             * 删除
             * @param key
             * @returns {Promise<boolean|*>}
             */
            async delResourceCache(key) {
                return await this._del(this.RESOURCES_CACHE_STORE_NAME, key);
            },
        
            /**
             * 清空
             * @returns {Promise<unknown>}
             */
            async clearAllResourceCache() {
                let store = await this._transactionStore(this.RESOURCES_CACHE_STORE_NAME);
                let request = store.clear();
            
                return await new Promise((ok) => {
                    request.onerror = function (e) {
                        console.warn('[SuperCache] clear res fail', e.target.error, e);
                        ok(false);
                    };
                
                    request.onsuccess = function (e) {
                        ok(true);
                    };
                });
            },
        
            /**
             * 等待indexedDB就绪
             * @returns {Promise<unknown>|Promise<boolean>}
             */
            async waitingForReady() {
                let superCache = this;
            
                if(this.db) {
                    return Promise.resolve(true);
                }
            
                let readyResult = await new Promise((ready) => {
                    let indexedDB = this.indexedDB();
                
                    if(!indexedDB) {
                        ready(false);
                        return false;
                    }
                
                    let request = indexedDB.open('byview', superCache.VERSION);
                
                    request.onerror = function(e) {
                        console.warn('byview indexedDB open fail', e.target.error, e);
                        ready(false);
                    };
                
                    request.onsuccess = function(e) {
                        superCache.db = e.target.result;
                        ready(true);
                    };
                
                    request.onupgradeneeded = function(e) {
                        let db = e.target.result;
                        let upgradeProcessors = [
                            {
                                ver: 2, run() {
                                    db.createObjectStore(superCache.COMM_CACHE_STORE_NAME, {keyPath: "id"})
                                    db.createObjectStore(superCache.RESOURCES_CACHE_STORE_NAME, {keyPath: "id"})
                                }
                            }
                        ];
                    
                        let oldVer = e.oldVersion;
                        let newVer = e.newVersion;
                        let processors = [];
                    
                        for(let key in upgradeProcessors) {
                            let processor = upgradeProcessors[key];
                        
                            if(processor.ver > oldVer && processor.ver <= newVer) {
                                processors.push(processor);
                            }
                        }
                    
                        processors = processors.sort((cur, next) => {
                            return cur.ver > next.ver ? 1 : -1;
                        });
                    
                        for(let i=0; i<processors.length; i++) {
                            console.log('[SuperCache] upgrade to ver', processors[i].ver);
                            processors[i].run();
                        }
                    };
                });
            
                return readyResult;
            },
        
            async checkResVersion() {
                if($byview.config.enabledScriptSuperCache && this.available()) {
                    let ver = localStorage.getItem('BYVIEW_RES_VERSION');
                
                    if(!ver || ver != window.BYVIEW_RES_VERSION) {
                        console.log("%c[SuperCache] ", 'color:gray;', "!!资源版本不一致, 清空资源缓存 ver=", ver, 'new=', window.BYVIEW_RES_VERSION);
                        await this.clearAllResourceCache();
                    
                        if(window.BYVIEW_RES_VERSION) {
                            localStorage.setItem('BYVIEW_RES_VERSION',window.BYVIEW_RES_VERSION);
                        }
                    }
                }
            }
        });
    })();
    
    //$byview
    return (_=>{
        let
            _appSign = '',
        
            //资源,脚本,样式,组件缓存
            _loadedScripts = {},
            _executedScripts = {},
            _executedScriptResult = {},
            _loadedResources = {},
            _loadedResourceCache = {},
            _loadedComponents = {},
            _loadedCss = {},
        
            // 样式范围限定
            _scopedAttrName = 'V',
            _byviewHasReady = false,
            _timeout = {},
            _interval = {},
            //全局值, 不区分vue文件
            _globalData = {},
            //私有值, 针对vue文件生效
            _privateData = {}
        ;
    
        let _events = {};
        let _startedApp = {};
        let _debugParams = null;
    
        window.$byview = window.$byview ? window.$byview : {};
        window.bv = window.bv ? window.bv : window.$byview;
        
        $byview._debugData = {
            _appSign, _loadedScripts, _executedScripts, _executedScriptResult, _loadedResources,
            _loadedResourceCache, _loadedComponents, _loadedCss, _scopedAttrName, _byviewHasReady,
            _events, _startedApp, _globalData, _privateData, _timeout, _interval
        };
    
        /**
         * 注册定时器, 定时器在app切换的时候自动失效
         * @param callback
         * @param delay
         * @param name
         * @param type
         * @returns {boolean|*}
         */
        function reg_timer(callback, delay, name, type) {
            delay = delay || 1;
            name = name || util.createUnqid('scopedTimer');
        
            let appComponent = $byview.appComponent;
            if(!appComponent) {
                return false;
            }
        
            if(typeof callback != 'function') {
                return false;
            }
        
            let timerContainer = _timeout;
        
            if(type == 'interval') {
                timerContainer = _interval;
            }
        
            let cfg = timerContainer[appComponent];
        
            if(!cfg) {
                cfg = timerContainer[appComponent] = {};
            }
        
            if(cfg[name]) {
                if(type == 'interval') {
                    clearInterval(cfg[name].timer);
                } else {
                    clearTimeout(cfg[name].timer);
                }
            }
        
            cfg[name] = {
                callback,
                delay,
                name,
                timer: type == 'interval' ?
                    setInterval(callback, delay)
                    : setTimeout(callback, delay)
            };
        
            return cfg[name];
        }
    
        /**
         * 取消注册指定定时器
         * @param name
         * @param type
         * @returns {boolean}
         */
        function unreg_timer(name, type) {
            let appComponent = $byview.appComponent;
            if(!appComponent) {
                return false;
            }
        
            let timerContainer = _timeout;
        
            if(type == 'interval') {
                timerContainer = _interval;
            }
        
            let cfg = timerContainer[appComponent];
        
            if(!cfg) {
                cfg = timerContainer[appComponent] = {};
            }
        
            if(!cfg) {
                return false;
            }
        
            if(!cfg[name]) {
                return false;
            }
        
            clearInterval(cfg[name].timer);
            delete cfg[name];
        }
    
        /**
         * 取消app所有注册定时器
         * @param comp
         * @param type
         * @returns {boolean}
         */
        function unreg_app_timer(comp, type) {
            if(!comp) {
                return false;
            }
        
            let timerContainer = _timeout;
        
            if(type == 'interval') {
                timerContainer = _interval;
            }
        
            let container = timerContainer[comp];
        
            if(!container) {
                return false;
            }
        
            for(let i in container) {
                if(container[i]) {
                    clearInterval(container[i].timer);
                }
            }
        
            delete timerContainer[comp];
        }
    
        /**
         * 取消app的所有注册
         */
        function unreg_app_timer_all() {
            let args = util.convertToArray(arguments);
            for(let i in args) {
                if(!args[i]) {
                    continue;
                }
                
                unreg_app_timer(args[i], 'interval');
                unreg_app_timer(args[i], 'timeout');
            }
        }
        
        Object.assign(window.$byview, {
            config: _config,
            webRootUrl: webRootUrl,
            baseUrl: byviewUrl,
            debugParams(raw) {
                if(!_debugParams) {
                    let data = {};
    
                    for(let i=0; i<localStorage.length; i++) {
                        let key = localStorage.key(i);
        
                        if(!/^BYVIEW_DEBUG_/.test(key)) {
                            continue;
                        }
        
                        let debugKey = key.replace(/^BYVIEW_DEBUG_/, '');
                        data[debugKey] = localStorage.getItem(key);
                    }
    
                    _debugParams = data;
                }
                
                if(raw) {
                    return _debugParams;
                } else {
                    if(_debugParams.enable) {
                        return util.clone(_debugParams);
                    } else {
                        return {};
                    }
                }
            },
            
            log() {
                if($byview.debugParams().enable) {
                    console.log.apply(console, util.convertToArray(arguments));
                }
            },
            
            groupLog() {
                if($byview.debugParams().enable) {
                    console.groupCollapsed.apply(console, util.convertToArray(arguments));
                    console.trace();
                    console.groupEnd();
                }
            },
        
            vueDebugParams() {
                return this.debugParams();
            },
        
            lastApp: null,
        
            //用于记录组件实例, 用于调试
            $components:{},
            //已解析好的组件
            _loadedComponents,
        
            /**
             * byview内的事件监听
             * @param eventName
             * @param callback
             * @param option
             * {
             *  id: null, //id
             *  }
             */
            on(eventName, callback, option) {
                option = option || {};
                
                if(!_events[eventName]) {
                    _events[eventName] = [];
                }
            
                let eventData = {
                    callback,
                    id:option.id
                };
                
                if(option.id) {
                    for (let i in _events[eventName]) {
                        let item = _events[eventName][i];
        
                        if (item.id == option.id) {
                            _events[eventName][i] = eventData;
                            return;
                        }
                    }
                }
                
                _events[eventName].push(eventData);
            },
        
            /**
             * byview事件触发
             * @param eventName
             * @param params
             * @returns {Promise<void>}
             */
            async fire(eventName, params) {
                if(!_events[eventName]) {
                    return;
                }
            
                let args = Array.prototype.slice.call(arguments);
                args.shift();
            
                for(let callbackInfo of _events[eventName]) {
                    if(callbackInfo.callback) {
                        await callbackInfo.callback(...args);
                    }
                }
            },
        
            /**
             * 等待页面资源的就绪
             * @returns {Promise<void>}
             */
            async ready() {
                if(_byviewHasReady) {
                    return ;
                }
                
                await superCache.checkResVersion();
            
                //等待dom就绪
                await new Promise(ok => {
                    if(['complete', 'loaded', 'interactive'].indexOf(document.readyState.toLowerCase()) != -1) {
                        ok()
                    } else {
                        document.addEventListener('DOMContentLoaded', ok);
                    }
                });
            
                //加载资源
                await $byview.loadScript($byview.config.loadScripts);
                _byviewHasReady = true;
                $byview.fire('ready');
                await $byview.loadCss($byview.config.loadCss);
            },
        
            /**
             * vueapp启动并在mount前调用
             * @param callback
             */
            beforeMount(callback) {$byview.on('beforeMount', callback);},
            onMount(callback) {$byview.on('mounted', callback);},
        
            /**
             * 启动以vue为基础的应用
             * @param baseComponent 基础组件路径
             * @param mountedSelector vue挂载的节点选择器, 可选属性, 若为空则自动取当前的内容容器
             * @param params 传递给初始组件setup中的参数
             * @param option 启动选项
             * @returns {Promise<void>}
             */
            async run(baseComponent, mountedSelector, params, option) {
                option = option || {};
                
                if(option.unregisterTimer) {
                    unreg_app_timer_all($byview.appComponent, baseComponent);
                }
                
                $byview.appSign = _appSign = util.parseUrlHash(location.href).sign;
                $byview.appComponent = baseComponent;
                
                let vueDebug = $byview.debugParams();
                //清空实例化的组件
                $byview.$components = {};
            
                if(vueDebug.enable) {
                    let warnMsg = msg => console.log('%c' + msg, "background:yellow;color:black;");
                
                    if(vueDebug.cleanConsole) {
                        console.clear();
                        warnMsg('=== 调试模式开启 ===');
                        warnMsg('自动清空控制台日志记录开启');
                    } else {
                        warnMsg('=== 调试模式开启 ===');
                    }
                
                    if(vueDebug.noCache) {
                        warnMsg('组件无缓存模式开启');
                        util._data._uniqidIncrements = {};
                        $byview._loadedComponents = _loadedComponents = {};
                        $byview.config.enabledScriptSuperCache = false;
                        document.querySelectorAll('style[compoment-scoped]').forEach(row => {
                            row.parentElement.removeChild(row);
                        });
                    }
                }
            
                let beforeRunParams = {baseComponent, mountedSelector, params};
                await $byview.fire('beforeRun', beforeRunParams, {sign:_appSign});
                baseComponent = beforeRunParams.baseComponent;
                mountedSelector = beforeRunParams.mountedSelector;
                params = beforeRunParams.params;
                
                let showLoading = !_startedApp[_appSign] || vueDebug.noCache;
                await $byview.loadScript([{url: byviewUrl + 'core.config.js',once: true, rankKey: true, cache:false}]);
                $byview.config.topframe && await $byview.loadScript([{url: byviewUrl + 'core/topframe.js', once: true}]);
                
                if(showLoading) {
                    $byview.topframe && $byview.topframe.loading('正在启动应用...');
                }
    
                //等待byview加载好
                await $byview.ready();
            
                let _reportError = (e, text) => {
                    console.error(e);
                    
                    if($byview.topframe) {
                        $byview.topframe.alertError(text);
                        $byview.topframe.closeLoading();
                    }
                };
            
                let mountedRoot = null;
            
                if(!mountedSelector) {
                    mountedRoot =  document.querySelector('#'+$byview.appSign);
                } else {
                    mountedRoot = typeof mountedSelector == 'string' ? document.querySelector(mountedSelector) : mountedSelector;
                }
            
                if(!mountedRoot) {
                    _reportError('挂载失败, 根元素不存在');
                    return;
                }
            
                let appElm = document.createElement('application');
                appElm.setAttribute(':params', 'params');
                mountedRoot.innerHTML = '';
                mountedRoot.appendChild(appElm);
            
                let [Application] = await $byview.loadComponent(baseComponent);
            
                if(!Application) {
                    console.error('vue加载失败: ' + baseComponent);
                    return;
                }
            
                //定义传递参数用的prop
                if(Application.props instanceof Array) {
                    if(Application.props.indexOf('params') == -1) {
                        Application.props.push('params');
                    }
                } else if(Application.props) {
                    if(!Application.props.params) {
                        Application.props.params = {default:{}};
                    }
                } else {
                    Application.props = ['params'];
                }
            
                params = params || {};
            
                let app = Vue.createApp({
                    components: {
                        Application
                    },
                
                    //传递给初始组件的参数
                    data() {
                        return {
                            params
                        }
                    },
                
                    created() {
                        window.Msg && Msg.close();
                    }
                });
    
                app._byviewRunParams = util.convertToArray(arguments);
                app.byviewRerun = _ => { $byview.run.apply($byview, app._byviewRunParams) };
                $byview.lastApp = app;
                
                await $byview.fire('beforeMount', app);
            
                if(showLoading) {
                    $byview.topframe && $byview.topframe.closeLoading();
                }
            
                app.mount(mountedRoot);
                await $byview.fire('mounted', app);
            },
        
            /**
             * 补全cms的url
             * @param url
             * @returns {*}
             */
            completeUrl(url) {
                return util.completeUrl(url, util.urlPath('/', $byview.config.baseUrl));
            },
        
            /**
             * 更新url版本
             * @param url
             * @returns {*}
             */
            updateUrlResver(url) {
                if($byview.config.resourceVersion) {
                    return util.updateUrlQuery(url, {
                        resver: $byview.debugParams().noCache ? util.randomStr() : $byview.config.resourceVersion
                    });
                }
            
                return url;
            },
        
            /**
             * 下载资源
             * @param resources 下载路径, 可以是一个包含多个路径或对象的数组,
             * 如果资源项是一个对象, 其格式为 {url:资源url, cache:true|false, randkey: true|false}
             * @param options 下载配置
             *  queue: 如果为true则排队回调
             *  cache: resource的默认值, 是否缓存
             *  randkey: resource的默认值,是否添加版本号参数到url中(随机字符)
             * @param callback 资源完成后的回调函数 (resObject, content) => {}
             * @returns {Promise<unknown>|Promise<[]>}
             */
            async download(resources, options, callback) {
                let opt = Object.assign({
                    cache: true,
                    randkey: false,
                    queue: true,
                }, options || {});
            
                if (Object.prototype.toString.call(resources)  != '[object Array]') {
                    resources = [resources];
                }
            
                resources = $byview.clone(resources);
            
                // 添加默认选项，并去掉已经执行过的脚本
                let ary = [];
                let originResource = resources;
            
                for (let i in originResource) {
                    if(typeof originResource[i] == 'string') {
                        originResource[i] = {url: originResource[i]};
                    }
                }
            
                for (let res of resources) {
                    if (!res) {
                        continue;
                    }
                
                    if (typeof res == 'string') {
                        res = {
                            url: res
                        };
                    }
                
                    if (res.url == '') {
                        continue;
                    }
                
                    res.originUrl = res.url;
                    res.url = $byview.completeUrl(res.url);
                    res = Object.assign({}, opt, res);
                
                    // 不需要缓存, 刷新状态, 使其重新下载
                    if (!res.cache) {
                        _loadedResources[res.url] = false;
                    }
                
                    ary.push(res);
                }
            
                resources = ary;
            
                let getFinishResult = () => {
                    let ary = [];
                
                    for(let res of originResource) {
                        if(!res.url) {
                            ary.push(null);
                        }
                    
                        ary.push(_loadedResourceCache[res.url]);
                    }
                
                    return ary;
                };
            
                // 没有需要下载的脚本, 直接完成
                if (resources.length == 0) {
                    return Promise.resolve(getFinishResult());
                }
            
                let syncLock = resources.length;
            
                return await new Promise(ok => {
                    // 全部下载完成
                    let finish = _ => ok(getFinishResult());
                
                    // 下载资源
                    let _download = async (res, complete) => {
                        let url = res.url;
                    
                        if(res.cache && $byview.config.enabledScriptSuperCache && superCache.available()) {
                            let cacheContent = await superCache.getResourceCache('file', url);
                        
                            if(cacheContent) {
                                complete && complete(res || {}, cacheContent);
                                return;
                            }
                        }
                    
                        let urlWithoutKey = url;
                        let xhr = new XMLHttpRequest;
                    
                        if(res.randkey) {
                            url = util.updateUrlQuery(url, {__resrk: util.createUnqid()});
                        }
                    
                        url = $byview.updateUrlResver(url);
                    
                        xhr.onreadystatechange = _ => {
                            if(xhr.readyState == XMLHttpRequest.DONE) {
                                let content = null;
                            
                                if(xhr.status == 200) {
                                    content = xhr.responseText;
                                } else {
                                    console.error(`下载 ${urlWithoutKey} 失败, 状态码=${xhr.status}`);
                                    complete && complete(res || {}, content);
                                    return;
                                }
                            
                                _loadedScripts[urlWithoutKey] = true;
                                complete && complete(res || {}, content);
                            
                                if(res.cache && $byview.config.enabledScriptSuperCache && superCache.available()) {
                                    superCache.setResourceCache('file', urlWithoutKey, content);
                                }
                            }
                        }
                    
                        xhr.open("GET", url, true);
                        xhr.send();
                    };
                
                    let calledIndex = -1;
                
                    //排队回调
                    let queueCall = _ => {
                        for(let i=calledIndex + 1; i<resources.length; i++) {
                            let res = resources[i];
                            let url = res.url;
                        
                            if(_loadedResources[url]) {
                                calledIndex ++;
                                callback && callback(res, _loadedResourceCache[url]);
                            } else {
                                break;
                            }
                        
                            if(i == resources.length-1) {
                                finish();
                            }
                        }
                    };
                
                    // 开始建立下载
                    for(let res of resources) {
                        let downloaded = _ => {
                            if(opt.queue) {
                                queueCall();
                            } else {
                                callback && callback(res, _loadedResourceCache[res.url]);
                            }
                        
                            syncLock --;
                        
                            if(syncLock == 0) {
                                finish();
                            }
                        };
                    
                        if(!$byview.debugParams().noCache && _loadedResources[res.url]) {
                            downloaded();
                        } else {
                            _download(res, (res, content) => {
                                _loadedResources[res.url] = true;
                                _loadedResourceCache[res.url] = content;
                                downloaded();
                            });
                        }
                    }
                }); //Promise
            },
        
            /**
             * 加载js文件
             * @param scripts js脚本路径, 可以为字符串也可以是一个数组或者一个对象
             *      如果是一个对象, 则支持如下属性:
             *          url: 脚本url地址, 如果是相对地址,则自动追加cms路径前缀, 如果是绝对地址,需要以 / 开头
             *          cache: 是否缓存,
             *          once: 是否只执行一次
             *          beforeExec: 下载内容后, 优先调用此回调函数, 若返回fasle给回调函数则不执行脚本
             *          randkey: 若为true则自动在脚本链接添加一个版本号以避免浏览器缓存, 默认为true
             *
             * @param options 默认加载选项, 子选项说明如下:
             *      queue: 默认值为true, 采用队列模式执行，按照数组顺序， 依次执行
             *      cache：script的默认cache选项, 是否使用缓存， 若为false，每次都从服务器下载脚本, 且每次下载都会更新本地缓存
             *      once: script的默认once选项, 如为true脚本则只执行一次
             *      beforeExec: beforeExec的默认选项
             *      assocResult: 若为true则返回的结果是一个关联url的对象, 默认为false, 表示按照url的顺序返回一个结果数组
             *      randkey: script的默认randkey选项
             *
             *      例子:
             *      awiat $byview.loadScript('1.js');  返回 [result1]
             *      awiat $byview.loadScript('1.js', {assocResult:true}); 返回 {'1.js': result1}
             *
             * @returns {Promise<unknown>} 可以通过awiat返回对应的script的结果
             */
            loadScript(scripts, options) {
                let opt = Object.assign({
                    queue: true,
                    cache: true,
                    once: false,
                    assocResult: false,
                    randkey: false
                }, options || {});
            
                if (Object.prototype.toString.call(scripts)  != '[object Array]') {
                    scripts = [scripts];
                }
            
                scripts = scripts.concat([]);
            
                for(let i in scripts) {
                    if(typeof scripts[i] == 'string') {
                        scripts[i] = {url: scripts[i]};
                    }
                
                    scripts[i] = Object.assign({}, opt, scripts[i]);
                }
            
                // 完事后执行脚本
                let _doExecScript = (scriptCfg, content) => {
                    if(typeof scriptCfg.beforeExec == 'function') {
                        if(!scriptCfg.beforeExec({
                            config: scriptCfg,
                            content: content
                        })) {
                            return;
                        }
                    }
                
                    let url = scriptCfg.url;
                
                    if (content != "") {
                        _executedScripts[url] = true;
                        _executedScriptResult[url] = util.eval(content, url);
                    }
                };
            
                return new Promise(ok => {
                    let finish = () => {
                        let result;
                    
                        if(opt.assocResult) {
                            result = {};
                        } else {
                            result = [];
                        }
                    
                        for(let i=0; i<scripts.length; i++) {
                            let url = $byview.completeUrl(scripts[i].url);
                        
                            if(opt.assocResult) {
                                result[scripts[i].url] = _executedScriptResult[url];
                            } else {
                                result.push(_executedScriptResult[url]);
                            }
                        }
                    
                        ok(result);
                    };
                
                    //下载脚本
                    $byview.download(scripts, opt, (res, content) => {
                        if(res.once && _executedScripts[res.url]) {
                            return;
                        }
                    
                        _doExecScript(res, content);
                    }).then(() => {
                        finish();
                    });
                });
            },
        
            /**
             * 解析vue内容转换成vue组件的配置对象
             * @param content
             * @param componentUrl
             * @param isParsed 标记content是否为已经解析好的对象
             * @returns {Promise<boolean|*>}
             */
            async parseVueContent(content, componentUrl, isParsed) {
                let componentId;
                let componentGenFuncContent = null;
                let componentGenFunc = () => {
                    return {};
                };
                let inlineStyles = [];
                let linkUrls = [];
                let compileScopeId;
                let templateContent;
    
                let real_url_to_array = function(urls, currentPath) {
                    if(util.isString(urls)) {
                        urls = [urls];
                    }
        
                    for(let i in urls) {
                        let component = urls[i];
            
                        if(component.substr(0, 2) == './') {
                            urls[i] = util.dirname(currentPath) + '/' + component.substr(2);
                        }
                    }
        
                    return urls;
                };
    
                //内置属性和方法
                let componentBuildIn = {
                    async byview_load_components(components, options) {
                        return await $byview.loadComponent(real_url_to_array(components, componentUrl), options);
                    },
        
                    async byview_load_name_components(components) {
                        return await $byview.loadComponentGroupByName(real_url_to_array(components, componentUrl));
                    },
        
                    async byview_load_script(scripts, options) {
                        return await $byview.loadScript(real_url_to_array(scripts, componentUrl), options);
                    },
                    
                    async byview_load_css(urls) {
                        return await $byview.loadCss(real_url_to_array(urls, componentUrl));
                    },
                    byview_get_gblprops: window.byview_get_gblprops,
                    byview_get_global_data:$byview.getGlobalData,
                    byview_set_global_data:$byview.setGlobalData,
                    byview_get_private_data:$byview.getPrivateData,
                    byview_set_private_data:$byview.setPrivateData,
                };
    
                $byview.fire('componentBuildIn', componentBuildIn, componentUrl);
                
                if(isParsed) {
                    componentId = content.componentId;
                    componentGenFuncContent = content.componentGenFuncContent;
                    inlineStyles = content.inlineStyles;
                    linkUrls = content.linkUrls;
                    templateContent = content.templateContent;
                    compileScopeId = _scopedAttrName + componentId;
                } else {
                    content = "<root>" + content + "</root>";
    
                    let root = new JKHTMLParser().parse(content);
                    let template = root.searchDirectChild({tags:['template']});
                    let scriptElms = root.searchDirectChildren({tags:['script']});
                    let scriptElm, scriptCount=0;
                    componentId = util.createUnqid();
    
                    // 没有设置type, 或者type为text/javascript的script作为脚本解析
                    // type为text/html的script作为模板解析
                    for(let i=0; i<scriptElms.length; i++) {
                        let type = scriptElms[i].getAttribute('type');
        
                        if(typeof type == 'undefined' || type === null || type == 'text/javascript') {
                            if(!scriptElm) {
                                scriptElm = scriptElms[i];
                            }
            
                            scriptCount ++;
                        }
                    }
    
                    if(scriptCount > 1) {
                        console.warn(`多个script标签在组件文件上: ${url}`);
                    }
    
                    if(scriptElm) {
                        let scriptContent = scriptElm.getInnerHTML();
                        let buildInFuncs = Object.keys(componentBuildIn).join(',');
                        componentGenFuncContent = `(async ({${buildInFuncs}}) => {` + scriptContent.replace(/(?<!(\/\/.*)|([a-zA-Z0-9_-]))export\s+default/, 'return ') + `})`;
                    }
    
                    //内联样式
                    let styles = root.searchDirectChildren({tags:['style']});
                    let hasScope=false, scopeId = util.createUnqid();
    
                    for(let i=0; i<styles.length; i++) {
                        let styletext = styles[i].getInnerHTML();
        
                        if(typeof styles[i].getAttribute('scoped') !== "undefined") {
                            hasScope = true;
                            styletext = $byview.compileScopedStyle(styletext, _scopedAttrName, componentId);
                        }
        
                        inlineStyles.push(styletext);
                    }
    
                    //外部样式文件
                    let links = root.searchDirectChildren({tags:['link']});
    
                    for(let i=0; i<links.length; i++) {
                        if(links[i].getAttribute('rel') != 'stylesheet' || !links[i].getAttribute('href')) {
                            continue;
                        }
    
                        let href = links[i].getAttribute('href');
    
                        if(href.substr(0, 2) == './') {
                            href = util.dirname(componentUrl) + '/' + href.substr(2);
                        }
        
                        //范围限定样式
                        if(typeof links[i].getAttribute('scoped') !== 'undefined') {
                            let [css] = await this.download(href);
                            let compiledCss = $byview.compileScopedStyle(css, _scopedAttrName, componentId);
            
                            if(compiledCss) {
                                inlineStyles.push(compiledCss);
                            }
            
                            continue;
                        }
                        
                        linkUrls.push(href);
                    }
    
                    compileScopeId = _scopedAttrName + componentId;
    
                    // 给模板的元素加上scoped属性
                    if(hasScope && template) {
                        let allElms = template.getAllChildElements();
                        for(let i=0; i<allElms.length; i++) {
                            if(!$byview.config.scopedIdIgnoreTags.includes(allElms[i].tag.toLowerCase())) {
                                allElms[i].setAttribute(compileScopeId, null);
                            }
                        }
                    }
    
                    templateContent = template ? template.getInnerHTML() : '';
                }
            
                if(componentGenFuncContent) {
                    componentGenFunc = util.eval(componentGenFuncContent, componentUrl);
                }
                //生成组件
                let component = await componentGenFunc(componentBuildIn);
                component.template = templateContent;
            
                if(templateContent == '' && !component.render) {
                    component.render = _ => {};
                }
            
                if(component.data && typeof component.data != 'function') {
                    component.data = eval('(_ => (return '+ JSON.stringify(component.data) +'))');
                }
                
                if(!component.name) {
                    component.name = util.ucfirst(util.filename(componentUrl));
                }
            
                // 通过组件mounted加载样式文件或者内联样式
                // 若组件只下载或者未实例化挂载到dom中, 组件自带的内联样式和样式文件
                // 均不会添加到页面中
                let oldMounted = component.mounted;
                if(linkUrls.length || inlineStyles.length) {
                    component.___inlineStyles = inlineStyles;
                    component.___linkUrls = linkUrls;
                
                    component.mounted = function() {
                        if (linkUrls.length) {
                            $byview.loadCss(linkUrls);
                        }
                    
                        if (inlineStyles.length) {
                            let styleElmId = 'style_' + componentId;
                            let styleElm = document.getElementById(styleElmId);
                        
                            if (!styleElm) {
                                styleElm = document.createElement('style');
                                styleElm.setAttribute('id', styleElmId);
                                styleElm.setAttribute('component-scoped', true);
                                styleElm.innerHTML = inlineStyles.join("\n");
                                document.querySelector('head').appendChild(styleElm);
                            }
                        }
                    
                        oldMounted && oldMounted.call(this);
                    }
                }
            
                //记录相关实例, 便于调试
                let oldCreated = component.created;
                component.created = function() {
                    this._byviewCompileScopeId = compileScopeId;
                    let key = component.name ? component.name : 'NN' + componentId;
                    let index = util.createUnqid(key, 3);
                
                    if(!$byview.$components[key]) {
                        $byview.$components[key] = {};
                    }
                
                    $byview.$components[key][index] = this;
                    $byview.$components[key].last = this;
                    oldCreated && oldCreated.call(this);
                }
                
                if(util.isPromise(component.components)) {
                    console.warn(`组件 ${componentUrl} 的 components 配置是一个Promise对象, 是否在使用byview加载组件时忘记使用 await ?`)
                }
                
                if(!isParsed && !$byview.debugParams().noCache) {
                    let cacheObj = {
                        componentId,
                        componentGenFuncContent,
                        inlineStyles,
                        linkUrls,
                        templateContent,
                        compileScopeId
                    };
                    
                    if(superCache.available() && $byview.config.enabledScriptSuperCache) {
                        await superCache.setResourceCache('component', componentUrl, cacheObj);
                    }
                    
                }
            
                return component;
            },
        
            /**
             * @typedef IByview_loadComponentParam_options
             * @property {boolean} assocResult -  布尔值, 是否返回关联对象
             * @property {boolean} async -  布尔值, 是否为vue的异步组件
             * @property {boolean} showLoading -  布尔值, 是否显示loading
             * @property {IByview_loadComponentParam_options_callback} beforeLoad -  回调
             * @property {IByview_loadComponentParam_options_callback} afterLoad -  回调
             */
            /**
             * @callback IByview_loadComponentParam_options_callback
             * @param {String} component
             */
            /**
             * 异步加载vue的组件
             * @param components 可以是一个字符串指定组件的url位置, 若为相对路径, 则自动追加cms的路径
             *   也可以是一个数组, 包含多个组件的url
             *   vue组件文件以.vue结尾, 可以兼容vue-cli中的组件, 但与它有写区别:
             *
             *   - vue文件允许<link rel="stylesheet" href="url" /> 方式引入css文件(异步加载)
             *     style支持scoped属性
             *
             *   - vue文件中script代码, 兼容vue-cli中的vue语法, 采用export default导出组件配置。对于script内容的解析
             *    只是简单将第一个export default字符串转为return， 因此在正式使用export default前， 不能出现该关键字， 包括
             *    字符串中, 如果字符串中需要出现, 则可以使用"export\x20default"代替。可以出现在单行注释中, 单不能出现在块注释中
             *
             *   - vue文件中的script代码最终会转成一个async函数, 故script里面可以使用await关键字来获取其他组件, 例如:
             *
             *      let components = await $byview.loadComponentGroupByName([
             *          'byview/components/test',
             *          'app/test/tbutton',
             *      ]);
             *
             *      或者:
             *
             *      let [TestComp, TButton] = await $byview.loadComponent([
             *          'byview/components/test',
             *          'app/test/tbutton',
             *      ]);
             *
             *   - 如果需要将vue文件中的script单独至另外一个js文件(例如, js逻辑庞大, 不想与template混在一起)
             *     可以将script的逻辑用async函数包裹, 然后在vue文件的script中通过下面方式引入:
             *
             *      export default await (await $byview.loadScript('组件路径')).pop();
             *
             * @param {IByview_loadComponentParam_options} options
             *  assocResult: 布尔值, 是否返回关联对象
             *  async: 布尔值, 是否为vue的异步组件
             *  showLoading: 布尔值, 是否显示组件加载loading(仅在异步组件时有效)
             *  beforeLoad: 回调函数, 开始加载组件前回调
             *  afterLoad: 回调函数, 组件加载完成后回调
             * @returns {Promise<[]|boolean|*[]>} 返回一个数组, 包含components参数顺序的vue组件配置对象
             */
            async loadComponent(components, options) {
                if(!components) {
                    return []
                }
            
                options = options || {};
                options = Object.assign({
                    assocResult: false
                }, options);
            
                let isAsync = options.async;
                delete options.async;
            
                if(typeof components == 'string') {
                    components = [components];
                }
            
                let newComponents = [];
            
                for(let i in components) {
                    if(!/\.vue$/i.test(components[i])) {
                        newComponents.push(components[i]);
                        components[i] += '.vue'
                    }
                }
            
                let result;
            
                //动态组件
                if(isAsync) {
                    result = [];
                
                    for(let i in components) {
                        result.push(Vue.defineAsyncComponent(() => {
                            return new Promise(ok => {
                                if(options.showLoading) {
                                    $byview.topframe.loading((L.i18n && L.i18n.common && L.i18n.common.loading_component) || "加载组件...");
                                }
                            
                                options.beforeLoad && options.beforeLoad(newComponents[i]);
                            
                                $byview.loadComponent(components[i], options).then(ret => {
                                    options.afterLoad && options.afterLoad(newComponents[i]);
                                
                                    if(options.showLoading) {
                                        $byview.topframe.closeLoading();
                                    }
                                
                                    ok(ret[0]);
                                });
                            });
                        }));
                    }
                
                    return result;
                }
            
                if(options.assocResult) {
                    result = {};
                } else {
                    result = [];
                }
            
                let downloadingIndex = {};
                let needDownloadUrls = [];
                let NoComponentCache = $byview.debugParams().noCache;
            
                for(let url of components) {
                    let cmsurl = $byview.completeUrl(url);
                    let component = $byview.debugParams().noMemCache ? null : _loadedComponents[cmsurl];
                
                    if(component) {
                        if(options.assocResult) {
                            result[url] = component
                        } else {
                            result.push(component);
                        }
                    } else {
                        if(!options.assocResult){
                            result.push({});
                            downloadingIndex [cmsurl]= result.length-1;
                        }
                    
                        needDownloadUrls.push(url);
                    }
                }
            
                if(needDownloadUrls.length == 0) {
                    return result;
                }
                
                let cacheMissingUrls = needDownloadUrls;
                
                if(!$byview.debugParams().noCache && superCache.available()) {
                    cacheMissingUrls = [];
                    
                    for(let i in needDownloadUrls) {
                        let url = needDownloadUrls[i];
                        let cmsurl = $byview.completeUrl(url);
                        let componentContent = (superCache.available() && $byview.config.enabledScriptSuperCache) ? await superCache.getResourceCache('component', cmsurl) : null;
                        
                        if(!componentContent) {
                            cacheMissingUrls.push(url);
                            continue;
                        }
    
                        let component = await $byview.parseVueContent(componentContent, cmsurl, true);
                        _loadedComponents[cmsurl] = component;
    
                        if(options.assocResult) {
                            result[url] = component;
                        } else {
                            result[downloadingIndex[cmsurl]] = component;
                        }
                    }
                }
                
                let downComponents = await $byview.download(cacheMissingUrls, {cache:!NoComponentCache, queue:false, randkey:NoComponentCache});
            
                for(let i in cacheMissingUrls) {
                    let url = cacheMissingUrls[i];
                    let cmsurl = $byview.completeUrl(url);
                    let component = {};
                
                    if(downComponents[i]) {
                        component = await $byview.parseVueContent(downComponents[i], cmsurl);
                    }
                
                    _loadedComponents[cmsurl] = component;
                
                    if(options.assocResult) {
                        result[url] = component;
                    } else {
                        result[downloadingIndex[cmsurl]] = component;
                    }
                }
            
                return result;
            },
        
            /**
             * 返回以组件命名的一个对象, 如果组件没有name属性, 将忽略
             * @param componentResourceUrls
             * @returns {Promise<{}>}
             */
            async loadComponentGroupByName(componentResourceUrls) {
                let components = await $byview.loadComponent(componentResourceUrls);
                let result = {};
            
                for(let i in components) {
                    let component = components[i];
                
                    if (component.name) {
                        result[component.name] = component;
                    }
                }
            
                $byview.log('加载组件', result);
                return result;
            },
        
            /**
             * 加载css
             * @param urls
             * @returns {Promise<void>}
             */
            async loadCss(urls) {
                if(!urls) {
                    return;
                }
            
                if(typeof urls == 'string') {
                    urls = [urls];
                }
                
                for(let i=0; i<urls.length; i++) {
                    if(util.isString(urls[i])) {
                        urls[i] = {url:urls[i]};
                    }
                }
            
                let head = document.querySelector('head');
            
                for(let i in urls) {
                    let url = urls[i].url;
                    let inline = urls[i].inline;
                    let elm;
                    
                    if(!url) {
                        continue;
                    }
    
                    url = $byview.completeUrl(url);
                    
                    if(inline) {
                        if(_loadedCss[url]) {
                            if($byview.debugParams().noCache) {
                                head.removeChild(_loadedCss[url]);
                            } else {
                                continue;
                            }
                        }
                        
                        let [cssContent] = await $byview.download(url);
                        elm = document.createElement('style');
                        elm.setAttribute('data-path', url);
                        elm.innerHTML = cssContent;
                        head.appendChild(elm);
                    } else {
                        if(_loadedCss[url]) {
                            continue;
                        }
                        
                        url = $byview.updateUrlResver(url);
                        elm = document.createElement('link');
                        elm.setAttribute('rel', 'stylesheet');
                        elm.setAttribute('href', url);
                        head.appendChild(elm);
                    }
    
                    _loadedCss[url] = elm;
                }
            },
        
            /**
             * 父类派生子类, 用于组件之间继承
             * @param parentComponentUrl 父类组件url, 若父组件未加载则自动加载
             * @param childComponentConfig 一个对象, 保存子组件配置
             * @returns {Promise<void>} 生成一个新的对象
             */
            async deriveComponent(parentComponentUrl, childComponentConfig) {
                return Object.assign({}, childComponentConfig, (await $byview.loadComponent(parentComponentUrl))[0])
            },
        
            /**
             * 编译范围属性的css
             * @param styleText
             * @param scopedName
             * @param scopeId
             * @returns {*}
             */
            compileScopedStyle(styleText, scopedName, scopeId) {
                if (!styleText) {
                    return;
                }
            
                //保留 /*/scoped/*/ 注释用于特殊用途
                styleText = styleText.replace(/\/\*(?!\/).*?(?<!\/)\*\//g, '');
                // styleText = styleText.replace(/\/\*.*?\*\//g, '');
            
                let styleSelectorPrefix = `[${scopedName}${scopeId}]`;
                // let styleSelectorPrefix = `${scopedName}`;
            
                let segments = styleText.split('{');
                let compileStyle = segments.map(item => {
                    let selectorsSeg = item.split('}');
                    let selectors = selectorsSeg.pop();
                
                    // @media, @keyframe
                    if(/^\s+@/.test(selectors)) {
                        return item;
                    }
                
                    if(/\/\*\/scoped\/\*\//.test(selectors)) {
                        selectorsSeg.push(selectors.replace(/\/\*\/scoped\/\*\//g, styleSelectorPrefix));
                    } else {
                        let selectorAry = selectors.split(',');
                        selectorAry = selectorAry.map(selector => {
                            if(/^\s*$/.test(selector)) {
                                return selector;
                            }
                        
                            // return styleSelectorPrefix + selector;
                        
                            if(/::.*\s*$/.test(selector)) {
                                return selector.replace(/(::.*\s*)$/, styleSelectorPrefix + '$1');
                            }
                        
                            if(/:.*\s*$/.test(selector)) {
                                return selector.replace(/(:.*\s*)$/, styleSelectorPrefix + '$1');
                            }
                        
                            return selector.replace(/\s+$/, '') + styleSelectorPrefix;
                        });
                    
                        selectorsSeg.push(selectorAry.join(','));
                    }
                
                    return selectorsSeg.join('}');
                }).join('{');
            
                return compileStyle;
            },
        
            /**
             * @param obj
             */
            clone(obj) {
                return util.clone(obj);
            },
    
            /**
             * 注册interval, 仅当前app有效
             * @param callback
             * @param delay
             * @param name
             * @returns {boolean|*}
             */
            setInterval(callback, delay, name) {
                return reg_timer(callback, delay, name, 'interval');
            },
    
            /**
             * 注销interval
             * @param name
             * @returns {boolean}
             */
            clearInterval(name) {
                unreg_timer(name, 'interval');
            },
    
    
            /**
             * 注册定时器
             * @param callback
             * @param delay
             * @param name
             * @returns {boolean|*}
             */
            setTimeout(callback, delay, name) {
                return reg_timer(callback, delay, name, 'timeout');
            },
    
            /**
             * 注销interval
             * @param name
             * @returns {boolean}
             */
            clearTimeout(name) {
                unreg_timer(name, 'timeout');
            },
            
            
            /**
             * 设置全局数据, 浏览器不刷新均可使用
             * 可在各个app中使用
             * @param key
             * @param value
             * @returns {{}}
             */
            setGlobalData(key, value) {
                _globalData[key] = value;
                return $byview;
            },
    
            /**
             * 获取全局数据
             * @param key
             * @returns {*}
             */
            getGlobalData(key) {
                return _globalData[key];
            },
    
            /**
             * 删除全局数据
             * @param key
             */
            removeGlobalData(key) {
                delete _globalData[key];
            },
    
            /**
             * 若全局数据没有设置, 则将默认的value设置为全局数据并返回
             * 如果vlaue是一个函数, 则会调用函数并将函数的返回值作为全局数据的值设置进去并返回
             * 如果value函数中需要请求网络以获取数据，则value函数必须定义为async函数或者函数必须返回一个Promise
             * @param key
             * @param value
             * @returns {Promise<*>}
             */
            async getOrSetGlobalData(key, value) {
                let result = _globalData[key];
                
                if(util.isUndefined(result)) {
                    result = util.isFunction(value) ? await value(key) : value;
                    _globalData[key] = result;
                }
                
                return result;
            },
    
            /**
             * 设置私有数据, 私有指的是针对当前run的vue文件生效
             * @param key
             * @param value
             * @returns {{}}
             */
            setPrivateData(key, value) {
                let comp = $byview.appComponent;
                
                if(!comp) {
                    return $byview;
                }
    
                _privateData[comp] = _privateData[comp] ? _privateData[comp] : {};
                _privateData[comp][key] = value;
                return $byview;
            },
    
            /**
             * 获取私有数据
             * @param key
             * @returns {null|*}
             */
            getPrivateData(key) {
                let comp = $byview.appComponent;
    
                if(!comp) {
                    return null;
                }
                
                return _privateData[comp] ? _privateData[comp][key] : null;
            },
    
            /**
             * 删除私有数据
             * @param key
             * @returns {null}
             */
            removePrivateData(key) {
                let comp = $byview.appComponent;
    
                if(!comp) {
                    return null;
                }
                
                if(!util.isUndefined(_privateData[comp]) && !util.isUndefined(_privateData[comp][key])) {
                    delete _privateData[comp][key];
                }
            },
    
            /**
             * 若私有数据没有设置, 则将默认的value设置为私有数据并返回
             * 如果vlaue是一个函数, 则会调用函数并将函数的返回值作为私有数据的值设置进去并返回
             * 如果value函数中需要请求网络以获取数据，则value函数必须定义为async函数或者函数必须返回一个Promise
             * @param key
             * @param value
             * @returns {Promise<null>}
             */
            async getOrSetPrivateData(key, value) {
                let comp = $byview.appComponent;
    
                if(!comp) {
                    return null;
                }
    
                _privateData[comp] = _privateData[comp] || {};
                let result = _privateData[comp][key];
    
                if(util.isUndefined(result)) {
                    result = util.isFunction(value) ? await value(key) : value;
                    _privateData[comp][key] = result;
                }
    
                return result;
            },
        });
    
        
        //定义全局函数
        window.byview_get_gblprops= function() {
            let instance = Vue.getCurrentInstance();
            let app = instance ? instance.appContext : null;
            app = app ? app : $byview.lastApp;
            
            if(app) {
                return app.config.globalProperties
            }
    
            return null;
        };
        
        window.byview_load_script = async function(scripts, options) {return await $byview.loadScript(scripts, options)};
        window.byview_load_css = async function(urls) {return await $byview.loadCss(urls)};
        window.byview_load_components = async function(components, options) {return await $byview.loadComponent(components, options)};
        window.byview_load_name_components = async function(components) {return await $byview.loadComponentGroupByName(components)};
        window.byview_set_private_data = function(key, value) {return $byview.setPrivateData(key, value)};
        window.byview_get_private_data = function(key) {return $byview.getPrivateData(key)};
        window.byview_remove_private_data = function(key) {return $byview.removePrivateData(key)};
        window.byview_getorset_private_data = async function(key, value) {return await $byview.getOrSetPrivateData(key, value)};
        
        window.byview_set_global_data = function(key, value) {return $byview.setGlobalData(key, value)};
        window.byview_get_global_data = function(key) {return $byview.getGlobalData(key)};
        window.byview_remove_global_data = function(key) {return $byview.removeGlobalData(key)};
        window.byview_remove_global_data = function(key) {return $byview.removeGlobalData(key)};
        window.byview_getorset_global_data = async function(key, value) {return await $byview.getOrSetGlobalData(key, value)};
        return window.$byview;
    })();
})(typeof window !== 'undefined' ? window : global);
//# sourceURL=byview://byview/core.js
