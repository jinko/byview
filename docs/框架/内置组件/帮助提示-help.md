# 帮助提示 help

### 效果图示

![1](imgs/help-img1.png)
 

### 属性

|name|类型|说明|
|----|-----|---|
|title|string|帮助图标右侧的文本|
|iconColor|string|帮助图标颜色|

### 插槽

|name|类型|说明|
|----|-----|---|
|默认|string|弹出提示的文本|

### 使用示例 - 引入

 #### 路径
 ```
byview/components/base/help.vue
 ```

 #### 引入

```javascript
let components = $byview.loadComponentGroupByName(['byview/components/base/help.vue']);
```

### 使用示例 - 模板

 #### 单图标 + 提示内容

```html
 <by-help title="盲注范围可以使用单位 K、M、B"></by-help>
```

 #### 图标文字 + 提示内容
 
 ```html
  <by-help title="盲注范围可以使用单位 K、M、B">文字label</by-help>
 ```

 #### 只要文字和图标
 
 ```html
  <by-help>文字label</by-help>
 ```

