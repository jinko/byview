# 表格列筛选 column-filter

这个组件用来给表格头部增加筛选功能

### 实例样图

![1](imgs/column-filter.png)

### 属性

|name|类型|说明|
|----|-----|---|
|sortable|--|功能尚未实现 |
|defaultValue|string|默认值 |
|type|string|类型， 可选`text`、`radio`、`checkbox`、`date`, 默认为text|
|name|string|emit上传的名称|
|filterOptions|array|选择项`radio`、`checkbox`类型生效|
|dateType|string|日期类型`date`类型生效|
|dateStartPlaceholder|string|开始时间文本, `date`类型生效|
|dateEndPlaceholder|string|结束时间文本, `date`类型生效|
|defaultFiltered|string|默认选中的值|

#### type 说明

> `text` 为筛选文本内容
>
> `radio` 为单选筛选项
>
> `checkbox` 为多选筛选项
>
> `date` 为日期筛选项

### 使用示例
#### 路径
 ```
byview/components/base/column-filter.vue
 ```

 #### 引入

```javascript
let components = $byview.loadComponentGroupByName(['byview/components/base/column-filter.vue']);
```

#### 

```html
<by-column-filter defaultValue="" v-model="filter.id" @change="update_list">ID</by-column-filter>
```

