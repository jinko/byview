# 日期选择组件 date-picker

封装 ElementPlus 里的 el-date-picker 增加数值格式参数


### 属性

|name|类型|说明|
|----|-----|---|
|valueFormat|string|数值格式, 默认值: `datestr`, 详细说明参考下方 |

#### valueFormat 说明

> 数值格式用来定义传入、传出组件值的格式，el-date-picker 默认是一个js的Date对象实例，不方便实际使用
>
> 可选项如下:
>
> `date` js的Date对象（跟el-date-picker一样）
> `datestr` YYYY-MM-DD HH:mm:ss 格式的日期字符串
> `timestamp` 时间戳(秒)
> `mstimestamp` 毫秒时间戳

#### 其他参数
> 其他参数请参考 ElementPlus 的文档

### 使用示例

#### 路径
 ```
byview/components/base/date-picker.vue
 ```

 #### 引入

```javascript
let components = $byview.loadComponentGroupByName(['byview/components/base/date-picker.vue']);
```

#### 

```html
<by-date-picker
        v-model="dialog.edit.data.start_time"
        type="datetime"
        value-format="timestamp">
</by-date-picker>
```



