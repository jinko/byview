# util 插件

util 插件主要提供一个比较基础的函数集

调用方法：
```javascript
$byview.util.xxxx
```

内置的方法有以下：

#### getValueRawType()

获取变量的实际类型 等价于 `Object.prototype.toString.call` 的返回值


#### isArray()

判断是否为数组

#### isObject()

判断是否为对象

#### isRegExp()

判断是否为正则表达式

#### isPromise()

判断是否为Promise

#### isFunction()

判断是否为函数

#### isBoolean()

判断是否为布尔值

#### isNaN()

判断是否为NaN

#### isNumber()

判断是否为Number

#### isString()

判断是否为字符串

#### isNull()

判断是否为null值

#### isUndefined()

判断是否为undefined

#### isEmpty()

判断是否为空值, 空值为 `false`, `undefined`, `null`, `''`, `空数组`

#### isEmptyMixed()

判断是否为空数组或者空对象, 即 `[]` 或 `{}`

#### isFalseValue()

判断是否为非真数值, `空值` 或 `0`

#### isTrueValue()

isFalseValue 的相反值

#### getDefaultWhenEmpty()

- 原型 

```javascript
getDefaultWhenEmpty(v, defaultValue)
```

当v为空值时, 则取defaultValue的值

#### convertToArray()

将带有length属性的类型的值, 转换成数组, 例如函数中的 arguments

#### clone()

克隆对象

#### createUnqid()

生成唯一id

- 原型

```javascript
createUnqid(unidKey, incrementMinLength)
```

`unidKey` 指定key, 每个key有独立的自增id

`incrementMinLength` 自增id最小长度, 默认为3

#### eval()

执行js代码

#### formatUrl()

格式化url

- 示例

```javascript
formatUrl('http://sdsd///asd\\sdsd')
//返回结果: "http://sdsd/asd/sdsd"
```

#### urlPath()

拼接url路径

- 示例

```javascript
urlPath('abc', 'def');

//返回结果: 'abc/def'
```

#### completeUrl()

若url不是一个完整的url则补全url 

- 示例

```javascript
completeUrl('test', '/cms')
//返回结果: "/cms/test"

completeUrl('test/423', '/cms')
//返回结果: "/cms/test/423"

completeUrl('/test', '/cms')
//返回结果: "/test"

completeUrl('http://test', '/cms')
//返回结果: "http://test"
```

#### getUrlQuery()

获取url中的查询参数

- 示例

```javascript
location.href = 'http://vm7.boyaagame.com/cms/?api=1&server=2&sid=13&2#roleAndSkill:role';
getUrlQuery(location.href);

//返回结果: "api=1&server=2&sid=13&2"
```

#### parseUrlQuery()

解析查询参数

- 示例

```javascript
parseUrlQuery('api=1&server=2&sid=13&2');
//返回结果: {2: null, api: "1", server: "2", sid: "13"}
```


#### unparseUrlQuery()

组装查询参数

- 原型

```javascript
unparseUrlQuery(params, raw)
```

`params` 查询参数
`raw` 若为true则返回原始数组(未拼接&), 而不是字符串

- 示例

```javascript
unparseUrlQuery({2: null, api: "1", server: "2", sid: "13"});
//返回结果: "2&api=1&server=2&sid=13"

unparseUrlQuery({test:[123,456], test2:777});
//返回结果: "test[]=123&test[]=456&test2=777"

unparseUrlQuery({test:[123,456], test2:777}, true);
//返回结果: [{"key":"test[]","value":"123"},{"key":"test[]","value":"456"},{"key":"test2","value":"777"}]
```

#### getUrlHashQuery()

获取url hash里的查询参数

- 示例

```javascript
location.href = 'http://vm7.boyaagame.com/cms/?api=1&server=2&sid=13&2#roleAndSkill:role?a=1&b=22';
getUrlQuery(location.href);
//返回结果: "a=1&b=22"
```

#### parseUrlHash()

解析url hash

- 示例

```javascript
location.href = 'http://vm7.boyaagame.com/cms/?api=1&server=2&sid=13&2#roleAndSkill:role?a=1&b=22';
parseUrlHash(location.href);
//返回结果: {"sign":"roleAndSkill","path":"role","params":{"a":"1","b":"22"}}
```

#### unparseUrlHash()

组装 hash

- 示例

```javascript
unparseUrlHash({"sign":"roleAndSkill","path":"role","params":{"a":"1","b":"22"}});
//返回结果: "roleAndSkill:role?a=1&b=22"
```

#### updateUrlQuery()

更新url中的查询参数

- 原型

```javascript
updateUrlQuery(url, queryParams, noMerge) 
```

`url` 需要更新的url

`queryParams` 查询参数对象

`noMerge` 是否将查询参数覆盖原有的查询, 若为true, 则queryParams将直接覆盖原始查询(原来的查询将丢弃)

- 返回值

返回更新之后的url

- 示例

```javascript
updateUrlQuery('http://vm7.boyaagame.com/cms/?api=1&server=2&sid=13&2#roleAndSkill:role?a=1&b=22', {server:100, test:'tete'}) 
//返回结果: "http://vm7.boyaagame.com/cms/?2&api=1&server=100&sid=13&test=tete#roleAndSkill:role?a=1&b=22"

updateUrlQuery('http://vm7.boyaagame.com/cms/?api=1&server=2&sid=13&2#roleAndSkill:role?a=1&b=22', {server:100, test:'tete'}, true) 
//返回结果: "http://vm7.boyaagame.com/cms/?server=100&test=tete#roleAndSkill:role?a=1&b=22"
```

#### updateUrlHashSign()

更新url hash中的sign

- 原型

```javascript
updateUrlHashSign(url, sign) 
```

#### updateUrlHashPath()

更新url hash中的path

- 原型

```javascript
updateUrlHashPath(url, path) 
```

#### updateUrlHashQuery()

更新url hash中的查询参数

- 原型

```javascript
updateUrlHashPath(url, params, noMerge) 
```

参数说明参考 [updateUrlQuery](#/函数速查.md?updateUrlQuery)


#### randomStr()

随机字符串

#### getBrowserInfo()

获取浏览器信息

 - 示例
 
 ```javascript
getBrowserInfo()
//返回结果: {"type":"chrome","version":"86.0.4240.198","versionSegments":[86,0,4240,198]}
```

#### antiShake()

生成防抖函数

- 原型

```javascript
antiShake(callback, option)
```

`callback` 回调函数
`option` 防抖参数

> `option.delay` 防抖延迟, 默认300毫秒

 - 返回值
 
返回包裹了防抖的函数

 - 示例

```javascript
let callback = antiShake(_ => console.log(1));

//连续执行两次
callback();callback();
//控制台输出1次 1

setTimeout(callback, 300);setTimeout(callback, 600);
//控制台输出2次1
```

#### formatData()

格式化前端数据(根据结构字段, 数值类型等约定)

- 原型

```javascript
formatData(format, data, removeKey)
```

`format` 格式
`data` 数据
`removeKey` 布尔值, 强制按照format格式里的key, 移除不在format里存在的key

>若format的格式如果是空对象 `{}` 则会忽略removeKey
 若非空对象, 例如 `{test:1}` 则指定removeKey为true的情况下, 该对象只能有test一个key
 其他key会被移除
>若format中的数组指定了成员, 例如 `[1,2,2]` 则在removekey 下, 数组长度固定

>若数组第一个成员为函数, 则认为该函数的返回值为数组对象成员格式化参数, 例如 `[_=> ({test:1})]`
 表示数组中的所有成员都是 `{test:1}` 格式
若格式中的对应有 `'__'` 成员且是函数的话, 那么该对象会以该函数返回值作为所有成员的格式(用于针对,动态键值的对象)
示例: 
>```javascript
>format= {
>    __: _=>({score:1, test:''})
>}
>formatData(format, {a:1}) => {a:{score:1, test:''}}
> ```
>
>format里所有值的数据为默认数据, 当格式不满足时会取默认数据
>  Number和String类型数据不会严格判断类型, 会直接转换类型, 包括null, true, false

#### dirname()

返回上级目录

- 示例

```javascript
dirname('a/b/c')
//返回结果: "a/b"
```

#### ucfirst()

首字母大写

#### lcfirst()

首字母小写

#### basename()

类似php basename

 - 示例

```javascript
basename('a/b/test.txt')
//返回结果: "test.txt"
```

#### filename()

获取文件名(无后缀)

 - 示例

```javascript
filename('a/b/test.txt')
//返回结果: "test"
```

#### pathWithoutSubfix()

获取路径(不带文件后缀)

 - 示例

```javascript
filename('a/b/test.txt')
//返回结果: "a/b/test"
```

#### copyText()

复制文本
