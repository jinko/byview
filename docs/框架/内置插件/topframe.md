# topframe

topframe 将旧系统的功能集中在这里供 byview 里面进行使用, 如果顶层没有旧的系统框架, 则不需要实现此文件

> 例如, 当将byview用在一个基于jQuery的老系统上, 且与老系统共存, 则可以动态加载byview, 并显示老系统的loading 

调用方法：
```javascript
$byview.topframe.xxxx
```

#### loading()

上层框架的loading

- 原型

```javascript
loading(text)
```


#### closeLoading()

上层框架关闭loading

#### alertError() 

上层弹出错误
