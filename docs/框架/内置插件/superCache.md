# superCache 插件

`superCache` 是集成在byview中的一个基于indexedDB的数据库缓存。不同于 `localStorage` 的是, 缓存内容可以很大。

调用方法：
```javascript
$byview.superCache.xxxx
```


#### available()

判断是否支持 supercache


#### get()

获取缓存数值

 - 原型
 
 ```javascript
async get(key, defaultValue) 
```

`key` 缓存key
`defaultValue` 默认值

#### set()

设置缓存

 - 原型
 
 ```javascript
async set(key, value, duration, falseWhenNoExist) 
```

`key` 缓存key
`value` 缓存值
`duration` 缓存有效时长(秒), 不传或者传-1表示永久有效

#### del()

设置缓存

 - 原型
 
 ```javascript
async del(key) 
```

`key` 缓存key
