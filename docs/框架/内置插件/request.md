# request 请求插件

用来请求数据的插件

调用方法：
```javascript
$byview.request.xxxx
```


#### send()

发送异步请求

- 原型

```javascript
async send(option)
```

- 参数

option 成员:

`method` 请求方法, 支持 get、 post、 jsonp, 默认为get
`data` 请求数据， 若请求方法为 post 则可以是`FormData`类型。如果是get请求， 则自动会吧参数追加到url中
`url` 请求url
`returnType` 数据返回类型, 可选址 json 和 plain. json 则返回json对象, plain则直接返回响应文本, 默认为json
`id` 设置请求id, 可以通过 getXHR 方法根据id获取请求的xhr实例, 请求类型为jsonp不可用
`init` 初始化回调函数, 用来初始化xhr实例, 请求类型为jsonp不可用 
`onProgress` 进度条回调函数, 用于上传文件时获取进度信息

> **onProgress** 的参数信息成员信息
> `lengthComputable` 下载长度是否明确(不明确无法显示进度条)
> `loaded` 已加载字节
> `total` 总长度
> `speed` 下载速度(Byte/s)
> `percent` 下载进度 0.00~100.00
> `percentStr` 下载进度(字符串) 0.00% ~ 100.00% 
> `originEvent` 原始事件对象

- 返回值

返回对应的响应结果


- 示例

```javascript
let result = async $byview.request.send({
    url: 'xxxxxx',
});
```

#### get()

send get请求的便捷版本

- 原型

```javascript
async get(url, params, option)
```

`url` 请求url
`params` 请求参数(自动合并到url中)
`option` send方法的option

- 示例 

```javascript
let result = async $byview.request.get(url);
```

#### post()

send post请求的便捷版本

- 原型

```javascript
async post(url, data, option)
```

`url` 请求url
`data` post请求参数
`option` send方法的option

- 示例 

```javascript
let result = async $byview.request.post(url, {id:1});
```

#### jsonp()

send jsonp请求的便捷版本

- 原型

```javascript
async jsonp(url, params, option)
```

`url` 请求url
`params` 请求参数(自动合并到url中)
`option` send方法的option

- 示例 

```javascript
let result = async $byview.request.jsonp(url);
```


