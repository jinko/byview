# 关于Vue

byview中使用的vue版本为vue3。vue3新增了composition api方式开发组件，与vue2传统的options api的区别是，composition api所有初始化都放在`setup`函数中，通过返回一个对象给vue实例提供属性函数等。所有的事件都在setup中实现。这样便于开发者将相关功能代码都放在同一个区域中，避免原来options api中的到处滚动屏幕编写javascript业务代码。故在byview的组件和业务开发中，推荐composition api方式进行开发。

## vue 两种模式的等价示例

- `options api`

```html
<!-- 模板 -->
<template>
    <div>
        text:<span class="text">{{helloText}}</span><br>
        <input type="text" ref="inputTest" v-model="helloWorld">
    </div>
</template>

<!-- 脚本 -->
<script>
    export default {
        name: 'TestDemo2',
        
        data: _ => ({
            helloText: '',
        }),
        
        mounted() {
            //属性
            this.helloText = '你好';
            
            //refs
            this.$nextTick(() => {
                this.$refs.inputTest.focus();
            });
            
            setTimeout(() => {
                this.helloText = "你好啊";
            }, 2000);
        },
        
        computed: {
            helloWorld() {
                return this.helloText + ', 世界!';
            }
        }
    }
</script>

<!-- 限定当前组件的样式 -->
<style scoped>
    .text {
        color: green;
    }
</style>
```

- `composition api`


```html
<!-- 模板 -->
<template>
    <div>
        text:<span class="text">{{helloText}}</span><br>
        <input type="text" ref="inputTest" v-model="helloWorld">
    </div>
</template>

<!-- 脚本 -->
<script>
    const {computed, onMounted, nextTick, ref} = Vue;
    
    export default {
        name: 'TestDemo',

        setup() {
            let helloText = ref('你好');
            
            //计算属性
            let helloWorld = computed(_ => helloText.value + ', 世界!');
            
            //ref
            let inputTest = ref(null);
            
            //事件
            onMounted(_ => {
                nextTick(_ => {
                    inputTest.value.focus();
                });
                
                setTimeout(_ => {
                    helloText.value = "你好啊";
                }, 2000);
            });
            
            return {
                helloText,
                helloWorld,
                inputTest
            }
        },
    }
</script>

<!-- 限定当前组件的样式 -->
<style scoped>
    .text {
        color: green;
    }
</style>
```