(_=>{
    let topframe = $byview.topframe = $byview.topframe ? $byview.topframe : {};
    return Object.assign($byview.topframe, {
        log() {
            let args = Array.prototype.slice.call(arguments);
            console.log("%ctopframe call", "color:dodgerblue;", ...args);
        },
    
        /**
         * 原始loading功能, 当byview尚未就绪,可以使用原功能
         * @param text
         */
        loading(text) {
            if(window.Msg) {
                Msg.close();
                M.doing(text||L.loading);
            } else {
                topframe.log('Msg.loading', text);
            }
        },
    
        closeLoading() {
            if(window.Msg) {
                Msg.close();
            } else {
                topframe.log('Msg.close');
            }
        },
    
        alertError(text) {
            if(window.BY) {
                BY.dialog.err(text);
            } else {
                topframe.log('BY.dialog.err', text);
            }
        }
    });
})();
