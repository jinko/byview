let byviewUrl = $byview.baseUrl;

$byview.config.loadScripts = $byview.config.loadScripts.concat([
    {url: byviewUrl + 'plugin/vue.plugin.js',once: true},
]);

$byview.config.loadCss = $byview.config.loadCss.concat([
    {url: byviewUrl + 'plugin/style/core.css', inline: true},
    {url: byviewUrl + 'vendor/iconfont/iconfont.css', inline: false},
]);

//超级开启缓存
$byview.config.enabledScriptSuperCache = true;

//不需要增加scopeid的标签
//scopeid是用于将组件内的带有scoped属性样式局部化
//有些组件没有具体的实例元素, 无法自动继承到实例元素中
//还有一些不需要局部化的元素 比如 br(内置)
//在vue中将会出现警告 'Extraneous non-props attributes'
$byview.config.scopedIdIgnoreTags = $byview.config.scopedIdIgnoreTags.concat([
    //vue自带
    'component', 'keep-alive', 'template', 'transition', 'transition-group', 'slot',

    //element-plus 标签
    'el-tooltip', 'el-popover', 'el-picker', 'el-date-picker', 'el-popper', 'el-dialog',

    //自定义标签
    'by-help',
]);

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
if($byview.debugParams().noCache) {
    $byview.config.resourceVersion = $byview.util.randomStr();
    $byview.config.enabledScriptSuperCache = false;
}

